package cat.itb.dicerollerkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var dice1: ImageView
    lateinit var dice2: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rollButton = findViewById(R.id.roll_dice)
        dice1 = findViewById(R.id.left_dice)
        dice2 = findViewById(R.id.right_dice)

        rollButton.setOnClickListener {
            roll(dice1)
            roll(dice2)



            //resultTextView.setText(Random.nextInt(1,7).toString())


        }
    }
    fun roll (imageView: ImageView){
        val random1 = Random.nextInt(1, 7)


        when (random1) {
            1 -> imageView.setImageResource(R.drawable.dice_1)
            2 -> imageView.setImageResource(R.drawable.dice_2)
            3 -> imageView.setImageResource(R.drawable.dice_3)
            4 -> imageView.setImageResource(R.drawable.dice_4)
            5 -> imageView.setImageResource(R.drawable.dice_5)
            6 -> imageView.setImageResource(R.drawable.dice_6)

        }
        if (random1 ==6){

        }
    }
}